import numpy as np
import scipy.io
import pandas as pd
import os
import json
import random
from scipy.stats import pearsonr
os.environ["CUDA_VISIBLE_DEVICES"]="2"
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
import keras
from keras.layers import Input, Dense
from keras.models import Model ,load_model
from keras.models import Sequential
from keras.callbacks import ModelCheckpoint ,EarlyStopping
from keras.layers.wrappers import Bidirectional
from keras.utils.generic_utils import Progbar
from keras.layers.normalization import BatchNormalization
#from keras.utils.visualize_util import plot
from keras.layers import LSTM, Dropout, GRU, Convolution1D,  MaxPooling1D, Flatten,Reshape,CuDNNLSTM
from keras.layers import Input, Dense, Dropout, TimeDistributed, GlobalAveragePooling1D,Conv2D,MaxPooling2D
import sys
from keras.preprocessing.sequence import pad_sequences
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session  
config = tf.ConfigProto()  
config.gpu_options.allow_growth = True  
set_session(tf.Session(config=config)) 

import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
set_session(tf.Session(config=config))


def directory(extension):
 list_dir = []
 count = 0
 for file in Mfccfiles:
    
    if (file.find(extension)!=-1) and (count!=8): # eg: '.txt'
      count += 1
      list_dir.append(file)
 return list_dir

def subfiles(ext,fold):
        list_dir=directory(ext)
        l1=list_dir[0:2];l2=list_dir[2:4];l3=list_dir[4:6];l4=list_dir[6:]
        if (fold==0):
               l=[l1,l2,l3,l4]
        elif (fold==1):
                l=[l2,l3,l4,l1]
        elif(fold==2):
                l=[l3,l4,l1,l2]
        else:
                l=[l4,l1,l2,l3]
        return l  
def phoneme_rate(y,st,et):
      phn=pd.read_csv(Y_dir+y+'.phn',delimiter='\t',header=None)
      phoneme_list=[]
      time_phoneme_1=[]
      time_phoneme_2=[]
      for i in range(0,phn.shape[0]):
                phoneme_list.append(phn[0][i].split(" ")[2])
                time_phoneme_1.append(np.float64(phn[0][i].split(" ")[0])/16000)
                time_phoneme_2.append(np.float64(phn[0][i].split(" ")[1])/16000)
      sn=[]
      for i in range(0,len(time_phoneme_1)):
         if(time_phoneme_1[i]>[st]):
            sn.append(i-1)
            s=sn[0]
            break
      if(len(sn)==0):
          sn.append(len(time_phoneme_1)-1) 
          s=sn[0]
          
      en=[]
      for i in range(0,len(time_phoneme_2)):
          if(time_phoneme_2[i]>[et]):
             en.append(i) 
             e=en[0]
             break
      
      if(len(en)==0):
          en.append(len(time_phoneme_2)-1) 
          e=en[0]
      
      count=0
      for i in range(s,e+1):
          #vowels=[ 'AA', 'AE', 'AH', 'AO', 'AW', 'AX', 'AY', 'EH', 'ER', 'EY', 'IH', 'IY', 'OW', 'OY', 'UH', 'UW'];
          vowels = ['aa', 'ae', 'ah', 'ao', 'aw', 'aax', 'ax', 'axh', 'ax-h','axr','ay','eh','el','em','en','enx','eng','er','ey','ih','ix','iy','ow','oy','uh','uw','ux']

          if phoneme_list[i] in vowels:
             count=count+1 
      num_sy=count
      sen_dur=et-st
      syllab_rate=num_sy/sen_dur    
      return syllab_rate

def build_model():
   inputDim=39;NoUnits=256
   mdninput_Lstm = Input(shape=(None,inputDim))
   lstm_1=Bidirectional(CuDNNLSTM(NoUnits, return_sequences=True))(mdninput_Lstm)
   lstm_2a=Bidirectional(CuDNNLSTM(NoUnits, return_sequences=True))(lstm_1)
   lstm_2=Bidirectional(CuDNNLSTM(NoUnits, return_sequences=True))(lstm_2a)
   output1=TimeDistributed(Dense(12, activation='linear'))(lstm_2)
   output2=TimeDistributed(Dense(12, activation='relu'))(lstm_2)
   output=keras.layers.concatenate([output1,output2],axis=-1)
   #model=Model(inputs=mdninput_Lstm,outputs=output)
   input_shape= Reshape((100,24,1))(output)

   conv_1=  Conv2D(64, (3,3), padding='same', activation = 'relu') (input_shape)
   max_1=   MaxPooling2D(pool_size = 3)(conv_1)
   conv_2=  Conv2D(64, (3,3), padding='same', activation = 'relu') (max_1)
   batch_1= BatchNormalization()(conv_2)
   conv_3=  Conv2D(64, (3,3), padding='same', activation = 'relu') (batch_1)
   conv_4=  Conv2D(128, (3,3), padding='same', activation = 'relu') (conv_3)
   max_2=   MaxPooling2D(pool_size = 2) (conv_4)
   flat_1=  Flatten()(max_2)
   dens_1=  Dense(128,activation = 'relu') (flat_1)
   batch_2= BatchNormalization() (dens_1)
   drop_1=  Dropout(0.20)(batch_2)
   dens_2=  Dense(64,activation = 'relu') (drop_1)
   batch_3= BatchNormalization()(dens_2)
   drop_2=  Dropout(0.20) (batch_3)
   dens_3=  Dense(32,activation = 'relu')(drop_2)
   batch_4= BatchNormalization()(dens_3)
   drop_3=  Dropout(0.20) (batch_4)
   fin_output=  Dense(1,activation ='relu')(drop_3)
   vqa_model=Model(inputs=mdninput_Lstm ,outputs=fin_output)

   #vqa_model.summary()
   return vqa_model

#NoUnits=256 ######### DOUBT
EPSILON=0.0000001;
BatchSize=16;NoUnits=256
co=0;va=0;te=0
VAL_loss = []
train_pearson_coeff = []
val_pearson_coeff = []
test_pearson_coeff = []
y_train = []
y_val = []
y_test = []


X_dir='/home2/data/jyothi/Data/timit_data/timit_mfcc/'
Y_dir='/home2/data/jyothi/Data/timit_data/phn/'
Mfccfiles=sorted(os.listdir(X_dir))
phnf=sorted(os.listdir(Y_dir))
Batchsize=16;NoUnits=256
subj=[]
ran=np.arange(0,5040,8)
for f in range(len(ran)):
        d=ran[f]
        extension=Mfccfiles[d][4:9]
        subj.append(extension)

for fold in range(0,4): #  no. of folds 
        X_val=[];Y_val=[];X_train=[];Y_train=[];X_test=[];Y_test=[]
        for subf in range(0,3): # for sets train,test,val
            
            if (subf==0 ): # for train
                print('FOLD::'+str(fold+1)+'...................appending..' + str(subf)+'..list into X_train')
                for sub in range(0,len(subj)): # for all subjects
                        extension=subj[sub]
                        lsd=subfiles(extension,fold)
                        #print(lsd[0:2])
                        #print(len(list_dir))
                        for j in range(0,2):
                            for i in range(0,2):
                                #print(X_dir+lsd[j][i])
                                MFCC_G =np.loadtxt(X_dir+lsd[j][i]) 
                                co=co+1 
                                se=range(0,MFCC_G.shape[0],50)
                                for m in range(0,len(se)-2):
                                        temp=MFCC_G[se[m]:se[m+2]]
                                        X_train.append(temp)
                                        Y_train.append(phoneme_rate(lsd[j][i][0:-4],se[m]/100 ,se[m+2]/100))
                
            elif(subf==1):
                print('FOLD::'+str(fold+1)+'...................appending..' + str(subf)+'..list into X_val')
                for sub in range(0,len(subj)): # for all subjects
                        extension=subj[sub]
                        lsd=subfiles(extension,fold)
                        #print(lsd[0:2])
                        #print(len(list_dir))
                        for j in range(2,3):
                            for i in range(0,2):
                                #print(X_dir+lsd[j][i])
                                MFCC_G =np.loadtxt(X_dir+lsd[j][i]) 
                                va=va+1 
                                se=range(0,MFCC_G.shape[0],50)
                                for m in range(0,len(se)-2):
                                        temp=MFCC_G[se[m]:se[m+2]]
                                        X_val.append(temp)
                                        Y_val.append(phoneme_rate(lsd[j][i][0:-4],se[m]/100 ,se[m+2]/100))
            else:
                print('FOLD::'+str(fold+1)+'...................appending..' + str(subf)+'..list into X_test')
                for sub in range(0,len(subj)): # for all subjects
                        extension=subj[sub]
                        lsd=subfiles(extension,fold)
                        #print(lsd[0:2])
                        #print(len(list_dir))
                        for j in range(3,4):
                            for i in range(0,2):
                                #print(X_dir+lsd[j][i])
                                MFCC_G =np.loadtxt(X_dir+lsd[j][i]) 
                                te=te+1 
                                se=range(0,MFCC_G.shape[0],50)
                                for m in range(0,len(se)-2):
                                        temp=MFCC_G[se[m]:se[m+2]]
                                        X_test.append(temp)
                                        Y_test.append(phoneme_rate(lsd[j][i][0:-4],se[m]/100 ,se[m+2]/100)) 
        print(len(X_train),co)
        print(len(Y_train))  
        print(len(X_val),va)
        print(len(Y_val))               
        num_test=len(X_test)
        num_train = len(X_train)
        num_val = len(X_val)
        c = list(zip(X_train, Y_train))
        print('Shuffling X_train and Y_train')
        random.shuffle(c)
        X_train, Y_train= zip(*c)
        X_train= [x for x in X_train]
        Y_train= [x for x in Y_train]
        X_train=np.array(X_train);Y_train=np.array(np.expand_dims(Y_train,axis=1));X_val=np.array(X_val);Y_val=np.array(np.expand_dims(Y_val,axis=1));X_test=np.array(X_test);Y_test=np.array(np.expand_dims(Y_test,axis=1))
        print(X_train.shape)
        print(Y_train.shape)
        print('..compiling model')
        weights = '/home2/data/jyothi/models/Custom_Obj_Models_corr/cust_obj_aai_cdnn_mthd2_srpearscoeff__weights.h5'
        model=build_model()
        model.load_weights(weights) 
        model.summary()
        model.compile(optimizer='adam', loss='mse')
        fName1 = '/home2/data/jyothi/models/Custom_Obj_Models_corr/seen_update_both_aai_cdnn/'
        try:
             os.mkdir(fName1)
        except OSError as error:
            print(error)

        fName = fName1+'timit_fold_'+str(fold+1)

        print('..fitting model')
    
        checkpointer = ModelCheckpoint(filepath=fName + '_50.h5', verbose=0, save_best_only=True,period=1)
        checkpointer1 = ModelCheckpoint(filepath=fName + '_weights_50.h5', verbose=0, save_best_only=True, save_weights_only=True,period=1)
        earlystopper = EarlyStopping(monitor='val_loss', patience=5)
        history=model.fit(X_train,Y_train,validation_data=(X_val,Y_val),nb_epoch=40, batch_size=16,verbose=1,shuffle=True,callbacks=[checkpointer,checkpointer1,earlystopper])
        #with open(fName+'history.json', 'w') as f:json.dump(history.history, f)
        print('loading the best model ')
        sr_model=load_model(fName+'_50.h5')
        test_pred = np.zeros(num_test)
        val_pred = np.zeros(num_val)
        train_pred = np.zeros(num_train)
        test_pred = sr_model.predict(X_test)
        val_pred = sr_model.predict(X_val)
        train_pred = sr_model.predict(X_train)	
        print('Calculating Pearson Coefficient.....')
        test_corr, _ = pearsonr(np.squeeze(test_pred), np.squeeze(Y_test))
        test_pearson_coeff.append(test_corr)
    
        val_corr, _ = pearsonr(np.squeeze(val_pred), np.squeeze(Y_val))
        val_pearson_coeff.append(val_corr)

        train_corr, _ = pearsonr(np.squeeze(train_pred), np.squeeze(Y_train))
        train_pearson_coeff.append(train_corr)

        #print('True Test Values : ' + str(Y_test))
        #print('Test Predictions : ' + str(test_pred))
        print('Test Pearson Coefficient : ' + str(test_pearson_coeff))

        #print('True Val Values : ' + str(Y_val))
        #print('Val Predictions : ' + str(val_pred))
        print('Val Pearson Coefficient : ' + str(val_pearson_coeff))

        #print('True Train Values : ' + str(Y_train))
        #print('Train Predictions : ' + str(train_pred))
        print('Train Pearson Coefficient : ' + str(train_pearson_coeff))
	
        y_test.append(test_pred)
        y_val.append(val_pred)
        y_train.append(train_pred)
test_pearson_coeff = np.array(test_pearson_coeff)
test_pearson_coeff_avg = np.mean(test_pearson_coeff)
print('Test Pearson Coefficient Avg: '+str(test_pearson_coeff_avg))

val_pearson_coeff = np.array(val_pearson_coeff)
val_pearson_coeff_avg = np.mean(val_pearson_coeff)
print('Val Pearson Coefficient Avg: '+str(val_pearson_coeff_avg))

train_pearson_coeff = np.array(train_pearson_coeff)
train_pearson_coeff_avg = np.mean(train_pearson_coeff)
print('Train Pearson Coefficient Avg: '+str(train_pearson_coeff_avg))

fName_mat = '/home2/data/jyothi/models/Custom_Obj_Models_corr/seen_timit_update_both_aai_cdnn/'
try:
    os.mkdir(fName_mat)
except OSError as error:
    print(error)

scipy.io.savemat(fName_mat+'Ytest_pred_50.mat', {'Ytest_pred': y_test}, oned_as='row')
scipy.io.savemat(fName_mat+'test_coeff_50.mat', mdict={'test_coeff': test_pearson_coeff}, oned_as='row')
scipy.io.savemat(fName_mat+'Yval_pred_50.mat', {'Yval_pred': y_val}, oned_as='row')
scipy.io.savemat(fName_mat+'val_coeff_50.mat', mdict={'val_coeff': val_pearson_coeff}, oned_as='row')
scipy.io.savemat(fName_mat+'Ytrain_pred_50.mat', {'Ytrain_pred': y_train}, oned_as='row')
scipy.io.savemat(fName_mat+'train_coeff_50.mat', mdict={'train_coeff': train_pearson_coeff}, oned_as='row')

