import numpy as np
import scipy.io
import pandas as pd
import os
import json
import random
from scipy.stats import pearsonr
os.environ["CUDA_VISIBLE_DEVICES"]="1,2"
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
import keras
from keras.layers import Input, Dense
from keras.models import Model ,load_model
from keras.models import Sequential
from keras.callbacks import ModelCheckpoint ,EarlyStopping
from keras.layers.wrappers import Bidirectional
from keras.utils.generic_utils import Progbar
from keras.layers.normalization import BatchNormalization
#from keras.utils.visualize_util import plot
from keras.layers import LSTM, Dropout, GRU, Convolution1D,  MaxPooling1D, Flatten,Reshape,CuDNNLSTM
from keras.layers import Input, Dense, Dropout, TimeDistributed, GlobalAveragePooling1D,Conv2D,MaxPooling2D
import sys
from keras.preprocessing.sequence import pad_sequences
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session  
config = tf.ConfigProto()  
config.gpu_options.allow_growth = True  
set_session(tf.Session(config=config)) 
from keras import backend as K
import math

def find_corr(test_pred,Y_testaai):
    b = np.zeros((12,))
    c = 0
    for i in range(Y_testaai.shape[0]):
        a = []
        a = [pearsonr(Y_testaai[i][:,j],test_pred[i][:,j])[0] for j in range(12)]
        if not math.isnan(sum(a)):
            b = b + np.array(a)
            c = c + 1
    return b/c


def directory(extension):
 list_dir = []
 count = 0
 for file in Mfccfiles:
    
    if (file.find(extension)!=-1) and (count!=8): # eg: '.txt'
      count += 1
      list_dir.append(file)
 return list_dir

def subfiles(ext,fold):
        list_dir=directory(ext)
        l1=list_dir[0:2];l2=list_dir[2:4];l3=list_dir[4:6];l4=list_dir[6:]
        if (fold==0):
               l=[l1,l2,l3,l4]
        elif (fold==1):
                l=[l2,l3,l4,l1]
        elif(fold==2):
                l=[l3,l4,l1,l2]
        else:
                l=[l4,l1,l2,l3]
        return l  
def phoneme_rate(y,st,et):
      mat=scipy.io.loadmat(EmaDir+y)
      phoneme_list=[]
      time_phoneme_1=[]
      time_phoneme_2=[]

      for k in mat.keys():
        if not k.startswith('__'):
                phone=mat[k]['PHONES']
                sd=phone[0][0].shape
                for i in range(0,sd[1]):
                        phoneme_list.append(phone[0][0][0][i][0][0][0:2])
                        time_phoneme_1.append(phone[0][0][0][i][1][0][0])
                        time_phoneme_2.append(phone[0][0][0][i][1][0][1])
      sn=[]
      for i in range(0,len(time_phoneme_1)):
         if(time_phoneme_1[i]>[st]):
            sn.append(i-1)
            s=sn[0]
            break
      if(len(sn)==0):
          sn.append(len(time_phoneme_1)-1) 
          s=sn[0]
          
      en=[]
      for i in range(0,len(time_phoneme_2)):
          if(time_phoneme_2[i]>[et]):
             en.append(i) 
             e=en[0]
             break
      
      if(len(en)==0):
          en.append(len(time_phoneme_2)-1) 
          e=en[0]
      
      count=0
      for i in range(s,e+1):
          vowels=[ 'AA', 'AE', 'AH', 'AO', 'AW', 'AX', 'AY', 'EH', 'ER', 'EY', 'IH', 'IY', 'OW', 'OY', 'UH', 'UW'];
          if phoneme_list[i].upper() in vowels:
             count=count+1 
      num_sy=count
      sen_dur=et-st
      syllab_rate=num_sy/sen_dur    
      return syllab_rate


def cascaded_model():
   inputDim=39;NoUnits=256
   mdninput_Lstm = Input(shape=(None,inputDim))
   lstm_1=Bidirectional(CuDNNLSTM(NoUnits, return_sequences=True))(mdninput_Lstm)
   lstm_2a=Bidirectional(CuDNNLSTM(NoUnits, return_sequences=True))(lstm_1)
   lstm_2=Bidirectional(CuDNNLSTM(NoUnits, return_sequences=True))(lstm_2a)
   output1=TimeDistributed(Dense(12, activation='linear'))(lstm_2)
   output2=TimeDistributed(Dense(12, activation='relu'))(lstm_2)
   output=keras.layers.concatenate([output1,output2],axis=-1)
   model=Model(inputs=mdninput_Lstm,outputs=output)
   return model

def PSP_delta(feat, N):
    if N < 1:
        raise ValueError('N must be an integer >= 1')
    NUMFRAMES = len(feat)
    denominator = 2 * sum([i**2 for i in range(1, N+1)])
    delta_feat = np.empty_like(feat)
    padded = np.pad(feat, ((N, N), (0, 0)), mode='edge')   # padded version of feat
    for t in range(NUMFRAMES):
        delta_feat[t] = np.dot(np.arange(-N, N+1), padded[t : t+2*N+1]) / denominator   # [t : t+2*N+1] == [(N+t)-N : (N+t)+N+1]
    return delta_feat
    
Xind=np.arange(0,12,2)
Yind=np.arange(1,13,2)
def DeriveEMAfeats(EmaDataIp):
    #EMA: time X dim
    N=2
    vel=PSP_delta(EmaDataIp,N)
    acc=PSP_delta(vel,N)
    Mvel=np.sqrt(np.square(vel[:,Xind])+np.square(vel[:,Yind]))
    Macc=np.sqrt(np.square(acc[:,Xind])+np.square(acc[:,Yind]))
    DerEMA=np.concatenate((EmaDataIp,Mvel,Macc),axis=-1) 
  
    return DerEMA


def Get_Wav_EMA_PerFile(EMA_file,Wav_file,F):
    EmaMat=scipy.io.loadmat(EmaDir+EMA_file);
    #print(EmaDir+EMA_file)
    for k in EmaMat.keys():
        if not k.startswith('__'):
                #arr_nam=EmaMat[k]['NAME']
                arr_sig=EmaMat[k]['SIGNAL']
                arr_sig_del=np.delete(arr_sig,[0,6,8],1)
                for i in range((arr_sig_del.shape[1])):
                        if i==0:
                        #print("SIg: ",arr_sig_del[0][i])
                                arr_sig_del_cor=np.delete(arr_sig_del[0][0],[1,3,4,5],1)
                                if (np.any(np.isnan(arr_sig_del_cor))):
                                    print(EmaDir+EMA_file)
                                    arr_sig_del_cor = ma.masked_array(arr_sig_del_cor, mask=np.isnan(arr_sig_del_cor))
                        else:
                                temp=np.delete(arr_sig_del[0][i],[1,3,4,5],1)
                                if (np.any(np.isnan(temp))):
                                    print(EmaDir+EMA_file)
                                    temp= ma.masked_array(temp, mask=np.isnan(temp))
                                arr_sig_del_cor=np.hstack((arr_sig_del_cor,temp))
    art_data=arr_sig_del_cor
    #print(art_data.shape)
                       
    MeanOfData=np.mean(art_data,axis=0)
    #print(MeanOfData)
    art_data-=MeanOfData
    C=0.5*np.sqrt(np.mean(np.square(art_data),axis=0))
    Ema=np.divide(art_data,C) # Mean remov & var normailized
    #Ema = art_data
    [aE,bE]=Ema.shape
    
    EMAfv=DeriveEMAfeats(Ema)
    #print(F)
    #print F.type
    EBegin=np.int(BeginEnd[F,0]*100)
    EEnd=np.int(BeginEnd[F,1]*100)
    
    MFCC_G =np.loadtxt(MFCCpath+Wav_file[:-4]+'.txt')  #feats
    #print(MFCC_G.shape)
    TimeStepsTrack=EEnd-EBegin
    return EMAfv[EBegin:EEnd,:], MFCC_G[EBegin:EEnd,:],TimeStepsTrack # with out silence

#DataDir='/home/apiiit-rkv/Desktop/IISC_Internship/IEEE_EMA'

DataDir= '/home2/data/jyothi/Data/IEEE_EMA/'
Subs=['F01','F02','F03','F04','M01','M02','M03','M04']
#Subs=['F03']
RootDir='/home2/data/jyothi/Data/IEEE_EMA/'

y_trainaai = [];y_testaai = [];y_valaai=[]
X_trainaai=[];Y_trainaai=[]; 
X_valaai=[];Y_valaai=[]; 
X_testaai=[];Y_testaai=[]; 
print('Loading Training data')


for ss in range(0,len(Subs)):
    Sub=Subs[ss]
    print(Sub)
    WavDir=RootDir+'/'+Sub+'/wav/';
    EmaDir=RootDir+'/'+Sub+'/data/';
    BeginEndDir=RootDir+'/'+Sub+'/StartEndTime/';
    MFCCpath=RootDir+'/'+Sub+'/mfcc/' # CHANGED

    EMAfiles=sorted(os.listdir(EmaDir))
    Wavfiles=sorted(os.listdir(WavDir))
    StartStopFile=os.listdir(BeginEndDir)
    StartStopMAt=scipy.io.loadmat(BeginEndDir+StartStopFile[0])
    BeginEnd=StartStopMAt['BGEN']
    #window_size=500
    #print(len(EMAfiles),len(Wavfiles))
    F=5 # Fold No
    rnf= (0.8*len(EMAfiles)) + (0.1*len(EMAfiles))
    #print(rnf , .8*len(EMAfiles) , len(EMAfiles))
    for i in np.arange(0,len(EMAfiles)):
         #print(Wavfiles[len(EMAfiles)+i])
         #print(i)
         E_t,M_t,TT=Get_Wav_EMA_PerFile(EMAfiles[i],Wavfiles[i],i)
         #print(E_t.shape,M_t.shape)
         #W_t=W_t#[np.newaxis,:,:,:]
         E_t=E_t#[np.newaxis,:,:
         M_t=M_t#[np.newaxis,:,:]
         se=range(0,M_t.shape[0],50)
         for m in range(0,len(se)-2):
               temp=M_t[se[m]:se[m+2]]
               temp1=E_t[se[m]:se[m+2]]

               if (i <= 0.8*len(EMAfiles)):
                  X_trainaai.append(temp)
                  Y_trainaai.append(temp1)

               elif (i > .8*len(EMAfiles)  and  i<= rnf):

                  X_valaai.append(temp)
                  Y_valaai.append(temp1)

               else:  

                  X_testaai.append(temp)
                  Y_testaai.append(temp1)


num_test=len(X_testaai)
num_train = len(X_trainaai)
num_val = len(X_valaai)
X_trainaai=np.array(X_trainaai);
Y_trainaai=np.array(Y_trainaai)
X_valaai=np.array(X_valaai);
Y_valaai=np.array(Y_valaai)
X_testaai=np.array(X_testaai);
Y_testaai=np.array(Y_testaai)

print("before padding")
print("XTr:",X_trainaai.shape)

print("AAI",Y_trainaai.shape)
print("valXTr:",X_valaai.shape)

print("valAAI",Y_valaai.shape)
print("testXTr:",X_testaai.shape)

print("testAAI",Y_testaai.shape)

X_trainaai=pad_sequences(X_trainaai, padding='post',maxlen=100,dtype='float32')
Y_trainaai=pad_sequences(Y_trainaai, padding='post',maxlen=100,dtype='float32')
X_valaai=pad_sequences(X_valaai, padding='post',maxlen=100,dtype='float32')
Y_valaai=pad_sequences(Y_valaai, padding='post',maxlen=100,dtype='float32')
X_testaai=pad_sequences(X_testaai, padding='post',maxlen=100,dtype='float32')
Y_testaai=pad_sequences(Y_testaai, padding='post',maxlen=100,dtype='float32')

print("after padding")
print("XTr:",X_trainaai.shape)

print("AAI",Y_trainaai.shape)
print("valXTr:",X_valaai.shape)

print("valAAI",Y_valaai.shape)
print("testXTr:",X_testaai.shape)

print("testAAI",Y_testaai.shape)
print('..compiling model')

model=cascaded_model()
model.compile(optimizer='adam', loss = 'mse')

OutFileName='/home2/data/jyothi/models/Custom_Obj_Models_corr/cust_obj_aai_wo_cdnn_sp'
fName=OutFileName
print('..fitting model')
checkpointer = ModelCheckpoint(filepath=fName +'_best_50.h5', monitor='val_loss',save_best_only=True, verbose=0,save_weights_only=True,period=1)
callbacks = ModelCheckpoint((fName+'_weights_50.h5'), monitor='val_loss', verbose=0, save_weights_only=True, mode='auto', period=1)
earlystopper = EarlyStopping(monitor='val_loss', patience=5)
history=model.fit(X_trainaai,Y_trainaai,nb_epoch=40,validation_data=(X_valaai,Y_valaai), batch_size=16,verbose=1,shuffle=True,callbacks=[checkpointer ,callbacks,earlystopper])
  
model.load_weights(fName+'_best_50.h5')
test_pred = np.zeros(num_test)
val_pred = np.zeros(num_val)
train_pred = np.zeros(num_train)
test_pred = model.predict(X_testaai)
val_pred=model.predict(X_valaai)
train_pred = model.predict(X_trainaai)

fname='/home2/data/jyothi/models/Custom_Obj_Models_corr/'

testaai_pearson = find_corr(test_pred,Y_testaai)
trainaai_pearson = find_corr(train_pred,Y_trainaai)
valaai_pearson = find_corr(val_pred,Y_valaai)

print('Test Pearson Coefficient arti : ' + str(testaai_pearson))
print('Train Pearson Coefficient arti : ' + str(trainaai_pearson))
print('val Pearson Coefficient arti : ' + str(valaai_pearson))
print('Test Pearson Coefficient arti : ' + str(np.mean(testaai_pearson)))
print('Train Pearson Coefficient arti : ' + str(np.mean(trainaai_pearson)))
print('val Pearson Coefficient arti : ' + str(np.mean(valaai_pearson)))
scipy.io.savemat(fname+'Ytest_pred_arti_wo_cdnn_50.mat', {'Ytest_pred': y_testaai}, oned_as='row')
scipy.io.savemat(fname+'test_coeff_arti_wo_cdnn_50.mat', mdict={'test_coeff': testaai_pearson}, oned_as='row')
scipy.io.savemat(fname+'Ytrain_pred_arti_wo_cdnn_50.mat', {'Ytrain_pred': y_trainaai}, oned_as='row')
scipy.io.savemat(fname+'train_coeff_arti_wo_cdnn_50.mat', mdict={'train_coeff': trainaai_pearson}, oned_as='row')   
scipy.io.savemat(fname+'Yval_pred_arti_wo_cdnn_50.mat', {'Yval_pred': y_valaai}, oned_as='row')
scipy.io.savemat(fname+'val_coeff_arti_wo_cdnn_50.mat', mdict={'val_coeff': valaai_pearson}, oned_as='row')
